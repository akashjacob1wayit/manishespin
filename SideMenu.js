import PropTypes from 'prop-types';
import React, {Component} from 'react';

import {NavigationActions} from 'react-navigation';
import {ScrollView, Text, View,Image,StyleSheet,TouchableOpacity,Alert, AsyncStorage} from 'react-native';
import { StackNavigator ,DrawerActions} from 'react-navigation';


class SideMenu extends Component {

  constructor(props){
    super(props);


    this.state = {
      isVisible: false,
      name: '',
      compReg:'',
      vatRegString:'',
      Selected:'Home'
    }




  }

  componentDidMount() {

    AsyncStorage.getItem("name").then((value) => {
      console.log({"name": value});

      this.setState({name: value})
      console.log(this.name);

    }).done();

    AsyncStorage.getItem("vatReg").then((value) => {
      console.log({"vatReg": value});

      this.setState({vatRegString: value})
      console.log(this.vatRegString);

    }).done();

    AsyncStorage.getItem("reg").then((value) => {
      console.log({"reg": value});

      this.setState({compReg: value})
      console.log(this.compReg);

    }).done();





  }

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  loginAction=()=>{

    this.props.navigation.navigate("login")
    // Alert.alert('action called');


  }


  AccountAction=()=>{


    this.props.navigation.navigate("accounts")

  }


  tripHistoryAction=()=>{


    this.props.navigation.navigate("TripHistory")

  }

  homeAction=()=>{
    this.setState({Selected: 'Home'})
    // Alert.alert('action called');

    this.props.navigation.navigate("Main")

  }
  clickAction=()=>{

    this.props.navigation.navigate("invoice")
    // Alert.alert('action called');


  }
  referAction=()=>{

    this.props.navigation.navigate("referFriend")
    // Alert.alert('action called');


  }

  paymentAction=()=>{
  this.props.navigation.navigate("payment")
  //  this.props.navigation.navigate("mileage")
    // Alert.alert('action called');


  }
  supportAction=()=>{

    this.props.navigation.navigate("support")
    // Alert.alert('action called');


  }

  settingAction=()=>{

    this.props.navigation.navigate("setting")
    // Alert.alert('action called');


  }

  render () {
    return (
      <View style={{flex: 1}}>
        <View style={styles.container}>
            <Image
              source = {require('./Images/dummy.png')}
              style = {styles.imgStyle}
              onPress={this.loginAction}

              />
            <View style={{width:120,height: 100, flexDirection: 'column',backgroundColor: '#dedede',left: 20,top: 40}}><Text style={{fontSize: 18,top: 35}}>John Doe</Text>
            <View style={{width:120,height: 30, flexDirection: 'row',backgroundColor: '#dedede',left: 0,top: 40}}><Text style={{fontSize: 14,left: 0}}>4.5</Text><Image
              source = {require('./Images/star.png')}
              style = {{left:5,width:14,height:14,top:1}}
              /></View>
        </View>


        </View>
        <ScrollView>
          <TouchableOpacity activeOpacity={0.5} onPress={this.homeAction}><View style={{backgroundColor:this.state.sele == 'Home' ? "#f3f3f3" :'white',height: 40,flexDirection: 'row'}}><Image source={require('./Images/Accounts-Information.png')}
            style={{height: 20,width: 20 , marginTop: 10,marginLeft: 5}}/><Text style={{marginTop: 10,marginLeft: 15 , fontWeight: '300',fontSize: 16}}>Home</Text></View></TouchableOpacity>

          {this.state.isVisible == true ? <TouchableOpacity activeOpacity={0.5} onPress={this.homeCompanyAction}><View style={{backgroundColor: 'white',height: 40,flexDirection: 'row'}}><Text style={{marginTop: 10,marginLeft: 40 , fontWeight: '300',fontSize: 16}}>Company</Text></View></TouchableOpacity>: null }

          {this.state.isVisible == true ? <TouchableOpacity activeOpacity={0.5} onPress={this.homeShareholderAction}><View style={{backgroundColor: 'white',height: 40,flexDirection: 'row'}}><Text style={{marginTop: 10,marginLeft: 40 , fontWeight: '300',fontSize: 16}}>Shareholder</Text></View></TouchableOpacity>: null }

          <TouchableOpacity activeOpacity={0.5} onPress={this.AccountAction}><View style={{backgroundColor: this.state.sele == 'account' ? "#f3f3f3" :'white',height: 40,flexDirection: 'row'}}><Image source={require('./Images/Accounts-Information2.png')}
            style={{height: 20,width: 20 , marginTop: 10,marginLeft: 5}}/><Text style={{marginTop: 10,marginLeft: 15 , fontWeight: '300',fontSize: 16}}>Account's Information</Text></View></TouchableOpacity>

          <TouchableOpacity activeOpacity={0.5} onPress={this.tripHistoryAction}><View style={{backgroundColor: this.state.sele == 'trip' ? "#f3f3f3" :'white',height: 40,flexDirection: 'row'}}><Image source={require('./Images/Trips-History2.png')}
            style={{height: 20,width: 20 , marginTop: 10,marginLeft: 5}}/><Text style={{marginTop: 10,marginLeft: 15 , fontWeight: '300',fontSize: 16}}>Trips History</Text></View></TouchableOpacity>


          <TouchableOpacity activeOpacity={0.5} onPress={this.referAction}><View style={{backgroundColor: this.state.sele == 'refer' ? "#f3f3f3" :'white',height: 40,flexDirection: 'row'}}><Image source={require('./Images/Refer-a-friend2.png')}
            style={{height: 20,width: 20 , marginTop: 10,marginLeft: 5}}/><Text style={{marginTop: 10,marginLeft: 15 , fontWeight: '300',fontSize: 16}}>Refer a friend(Free Rides)</Text></View></TouchableOpacity>


          <TouchableOpacity activeOpacity={0.5} onPress={this.paymentAction}><View style={{backgroundColor: this.state.sele == 'payment' ? "#f3f3f3" :'white',height: 40,flexDirection: 'row'}}><Image source={require('./Images/Payment2.png')}
            style={{height: 20,width: 20 , marginTop: 10,marginLeft: 5}}/><Text style={{marginTop: 10,marginLeft: 15 , fontWeight: '300',fontSize: 16}}>Payment</Text></View></TouchableOpacity>


          <TouchableOpacity activeOpacity={0.5} onPress={this.supportAction}><View style={{backgroundColor: this.state.sele == 'support' ? "#f3f3f3" :'white',height: 40,flexDirection: 'row'}}><Image source={require('./Images/Support2.png')}
            style={{height: 20,width: 20 , marginTop: 10,marginLeft: 5}}/><Text style={{marginTop: 10,marginLeft: 15 , fontWeight: '300',fontSize: 16}}>Support</Text></View></TouchableOpacity>


          <TouchableOpacity activeOpacity={0.5} onPress={this.settingAction}><View style={{backgroundColor: this.state.sele == 'settings' ? "#f3f3f3" :'white',height: 40,flexDirection: 'row'}}><Image source={require('./Images/setting2.png')}
            style={{height: 20,width: 20 , marginTop: 10,marginLeft: 5}}/><Text style={{marginTop: 10,marginLeft: 15 , fontWeight: '300',fontSize: 16}}>Settings</Text></View></TouchableOpacity>



        </ScrollView>
        <View style={styles.footerContainer}>
          <Text></Text>
        </View>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};
const styles = StyleSheet.create({
  container: {
    backgroundColor:'#dedede' ,
    height:180,
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: 0,
    padding: 5

  },
  imgStyle:{
    top: 40,
    left:15 ,
    width:100,
    height:100,
    borderRadius: 50,
  },
  containText :{
    top: 10,
    flex: 1,
    textAlign: 'left',
    fontSize: 24,
    color: '#FF9F34',
  },
  containTextSmall :{
    top: 10,
    flex: 1,
    fontSize: 15,
    color: '#FF9F34',
    textAlign: 'left',
    left: 0

  },
  containTextMeduim :{
    top: 20,
    flex: 1,
    textAlign: 'left',
    fontSize: 20,
    color: '#FF9F34',
    left: 0,
  },

});

export default SideMenu;
