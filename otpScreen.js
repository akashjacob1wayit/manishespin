  /* @flow */

  import React, { Component } from 'react';
  import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    Image,
    TextInput,
    KeyboardAvoidingView,
    Button,
    TouchableOpacity,
    AsyncStorage,
    Alert
  } from 'react-native';
  const {width,height} = Dimensions.get('window')

  export default class otpScreen extends Component {
    constructor(props){
      super(props)

      this.state = {
          first: '',
          second: '',
          third:'',
          fourth:'',
          otpNew:'',
          otpMatch:'',
          userRefString:"",
          mobileNumber:"",
          clearInput:false


        }

        AsyncStorage.getItem("mob").then((value) => {
              console.log({"mob": value});

            this.setState({mobileNumber:value});


            }).done();

      }


    submitAction=()=>{

AsyncStorage.getItem("otp").then((value) => {
      console.log({"otp": value});
      var String_3 = this.state.first.concat(this.state.second);
      var String_4 = this.state.third.concat( this.state.fourth);
      var String_34 = String_3.concat(String_4);
      console.log(String_34);
if (value != String_34) {
  console.log(this.state.clearInput);
  this.setState({clearInput:true})

  Alert.alert("Incorrect Otp.");
  console.log(this.state.clearInput);

}else {

this.submitNEWAction();
}

    }).done();

  }

  submitNEWAction=()=>{

  AsyncStorage.getItem("userRef").then((value) => {
        console.log({"userRef": value});
        this.setState({userRefString:value});

        this.upLoadDocAction();


      }).done();

}

async upLoadDocAction(){



        var data = {
         status: "1",
         userRef:this.state.userRefString
        };
        try {
         let response = await fetch(
          "http://139.59.80.65/espin_api/sms/verifyotp",
          {
            method: "POST",
            headers: {
             "Accept": "application/json",
             "Content-Type": "application/json"
            },
           body: JSON.stringify(data)
         }
        );


         if (response.status >= 200 && response.status < 300) {

           const myArrStr = response._bodyInit;
           const newData = JSON.parse(myArrStr)
           console.log(newData);
           AsyncStorage.setItem("status","1");

           this.props.navigation.navigate('Item2')

        }else {
          Alert.alert("Alert","Try Again" );

        }
       } catch (errors) {
         Alert.alert(errors);
        }


  }


    backAction=()=>{
  //

      // Write your own code here, Which you want to execute on Floating Button Click Event.
      //Alert.alert("Floating Button Clicked");
      this.props.navigation.navigate('Item0')
    }
    // onTextChanged(t) { //callback for immediate state change
    //    if (t == 2) { this.setState({ autoFocus1: false, autoFocus2: true }, () => { console.log(this.state) }) }
    //    if (t == 3) { this.setState({ autoFocus2: false, autoFocus3: true }, () => { console.log(this.state) }) }
    //    if (t == 4) { this.setState({ autoFocus3: false, autoFocus4: true }, () => { console.log(this.state) }) }
    //  }
    render() {
      return (
        <View style={styles.container}>
          <View style={{backgroundColor: 'white',width: width,height: height/2,flexDirection: 'column',alignItems: 'flex-start'}}>
        <TouchableOpacity activeOpacity={0.5} onPress={this.backAction}><View  style = {{top:10,left:0, width: 60,height: 60,alignItems: 'center',backgroundColor:'white',justifyContent:'center'}}>
          <Image style = {{ left:-10,width: 12,height: 15,alignItems: 'center'}}
            source = {require('./Images/back-arrow.png')}/></View></TouchableOpacity>

          <View style = {{top:30,backgroundColor:'white',width:width,height:height/2 - 80,alignItems:'center',}}><Text style = {{fontSize:25,fontWeight:'400' , top:15}} >VERIFICATION CODE</Text>
            <Text style = {{fontSize:16,fontWeight:'200' , top:30}}>Enter the four digit code sent to you at</Text>

          <Text style = {{fontSize:16,fontWeight:'200' , top:40}}>{this.state.mobileNumber}</Text>

          <KeyboardAvoidingView behavior = 'padding' style = {styles.container}><View style = {{top:50,backgroundColor:'white',width:240,height:70,flexDirection:'row',}}>

            <TextInput placeholder = ""
              textAlign={'center'}
    onChangeText={first => {this.setState({first}), this.secondTextInput.focus()}}
    placeholderTextColor = 'black'
    autoCapitalize = 'none'
    returnKeyType = "next"
    maxLength={1}
    keyboardType = 'number-pad'
    autoFocus={true}
    ref={(input) => { this.firstTextInput = input; }}
    value={!this.state.clearInput ? this.state.first : null}
     style = {styles.input} />

     <TextInput placeholder = ""
       textAlign={'center'}
     onChangeText={second => {this.setState({second}), this.thirdTextInput.focus()}}
     placeholderTextColor = 'black'
     autoCapitalize = 'none'
     returnKeyType = "next"
     keyboardType = 'number-pad'
     maxLength={1}
     ref={(input) => { this.secondTextInput = input; }}
     value={!this.state.clearInput ? this.state.second : null}
     style = {styles.input} />

     <TextInput placeholder = ""
       textAlign={'center'}
     onChangeText={third => {this.setState({third}),this.fourthTextInput.focus()}}
     placeholderTextColor = 'black'
     autoCapitalize = 'none'
     maxLength={1}
     returnKeyType = "next"
     keyboardType = 'number-pad'
     ref={(input) => { this.thirdTextInput = input; }}
     value={!this.state.clearInput ? this.state.third : null}
     style = {styles.input}/>


     <TextInput placeholder = ""
       textAlign={'center'}
       onChangeText={fourth => {this.setState({fourth , clearInput:false}) , this.submitAction(),this.firstTextInput.focus()}}
     placeholderTextColor = 'black'
     autoCapitalize = 'none'
     returnKeyType = "next"
     keyboardType = 'number-pad'
     maxLength={1}
     ref={(input) => { this.fourthTextInput = input; }}
     value={!this.state.clearInput ? this.state.fourth : null}
     style = {styles.input} />

          </View></KeyboardAvoidingView>

        </View>

</View>

          <View style={{backgroundColor: 'white',width: width,height: height/2,flexDirection: 'column',alignItems: 'center',justifyContent: 'center'}}><Image style = {{width: width,height: height/2,alignItems: 'center'}} source = {require('./Images/BG-IMAGE.png')}/></View>
      </View>);
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    input :{
      height: 70,
      backgroundColor: '#EBEBF1',
      color: 'black',
      width: 50,
      marginLeft: 5,
      marginRight: 5,

    },
  });
