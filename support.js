/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Linking,
  KeyboardAvoidingView,
  TextInput,
  AsyncStorage,
  Alert

} from 'react-native';
import login from './login'
const {width,height} = Dimensions.get('window')
import { DrawerActions } from 'react-navigation';

const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
export default class support extends Component {

  constructor(props){
    super(props)

    this.state = {
      massageString:'',
      userRefString:'',
      passwordInput:''
      };

  }


  async postMassage() {

      var data = {
       message: this.state.massageString,
       userRef:this.state.userRefString
      };
      try {
       let response = await fetch(
        "http://139.59.80.65/espin_api/support",
        {
          method: "POST",
          headers: {
           "Accept": "application/json",
           "Content-Type": "application/json"
          },
         body: JSON.stringify(data)
       }
      );


       if (response.status >= 200 && response.status < 300) {

         const myArrStr = response._bodyInit;
         const newData = JSON.parse(myArrStr)
         console.log(newData);
         const newData1 = newData.data
         const newData21 = newData1[0]
         Alert.alert(
           'Success',
           'Message Sent Successfully',
           [
             {text: 'OK', onPress: () => {this.props.navigation.navigate('Main')}},
           ],
           { cancelable: false }
         )

      }else {
        Alert.alert("Alert","Try Again" );

      }
     } catch (errors) {
       Alert.alert(errors);
      }
  }

  SubmitAction=()=>{

    AsyncStorage.getItem("userRef").then((value) => {
          console.log({"userRef": value});
          this.setState({userRefString:value});
          if (!this.state.massageString) {
            Alert.alert("Alert","Enter Message" );

          }else{
          this.postMassage();

          }

        }).done();



  //  this.props.navigation.navigate('Main')
  }
  render() {
    return (
    <KeyboardAvoidingView behavior = 'padding' style = {styles.container}><View style={styles.container}>
        <View style={{top: 0,width: SCREEN_WIDTH,height: 70,flexDirection: 'row'}}><TouchableOpacity style={{marginTop: 35,marginLeft: 15}} onPress={()=>{this.props.navigation.dispatch(DrawerActions.openDrawer())}}><Image style={{width: 25 ,height: 20}} source={require('./Images/toggle.png')}/></TouchableOpacity>
        <View style={{width: width-60 ,justifyContent: 'center',alignItems: 'center',marginTop: 20,backgroundColor: '#f3f3f3'}}>
          <Text style={{color: 'black',justifyContent: 'center',alignItems: 'center',fontSize: 20,fontWeight: 'bold'}}>SUPPORT</Text>
        </View></View>
        <View style={{width: width,height: 1,backgroundColor: '#dedede',top: 0}}></View>
        <Text style={{left: 10,marginTop: 10,color: 'black'}}>Enter Message</Text>
          <View style = {styles.input2}><TextInput placeholder = ""
  placeholderTextColor = 'black'
  onChangeText={massageString => this.setState({massageString})}
   numberOfLines={20}
  returnKeyType = "go"
  multiline={true}
  ref = {(input)=> this.passwordInput = input}
  style={{width:SCREEN_WIDTH-20,height: 170, backgroundColor: 'clear'}}/></View>

<TouchableOpacity activeOpacity={0.5} onPress={this.SubmitAction}><View style={{width: 150,height: 40,borderWidth: 0.0,borderColor: 'grey',left: width/2 - 75,marginTop: 30,alignItems: 'center',justifyContent: 'center',backgroundColor: '#F27125'}}><Text style={{color: 'white'}}>Submit</Text></View></TouchableOpacity>

      </View></KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f3f3f3'
  },
  input2 :{
    backgroundColor: 'white',
    top:10,
    color: 'black',
    left:10,
    borderColor: 'lightgrey',
    borderWidth:1.5,
    width: SCREEN_WIDTH-20,
    height: 170,
    alignItems: 'flex-start',
    textAlign:'left',
    textAlignVertical: "top",
    justifyContent: 'flex-start',
    flexWrap:'wrap'
    },
});
