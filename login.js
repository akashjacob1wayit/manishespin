/**
* Sample React Native App
* https://github.com/facebook/react-native
*
* @format
* @flow
*/

import React, {Component} from 'react';
import {TouchableHighlight,Platform, StyleSheet, Text, View,Image,Dimensions,TextInput,TouchableOpacity,Alert,KeyboardAvoidingView,FlatList,AsyncStorage} from 'react-native';
import {createStackNavigator} from 'react-navigation';
import otpScreen from './otpScreen';
import ListPopover from 'react-native-list-popover';
const offset = (Platform.OS === 'android') ? -200 : 0;
const {width,height} = Dimensions.get('window')
const items = ['Item 1', 'Item 2','Item 1', 'Item 2', 'Item 2','Item 1', 'Item 2', 'Item 2','Item 1', 'Item 2', 'Item 2','Item 1', 'Item 2'];

const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
const ASPECT_RATIO = width/height
const AppNavigator = createStackNavigator({
otp : { screen :otpScreen },
},{initialRouteName: 'otpScreen' , headerMode:'none' }
)

type Props = {};
export default class login extends Component<Props> {

constructor(props){
  super(props)

  this.state = {
      isVisible: true,
      vHeight: 0,
      leftMNew:-SCREEN_WIDTH,
      loading: false,
      data: [],
      page: 0,
      seed: 1,
      error: null,
      refreshing: false,
      city:'Select City',
      bWidth:0.0,
      mNumber:'',
      cityId:'',
      tapStatus:false,
      mobileNumber:""
    };

}

// makeRemoteRequest = () => {
//
//
//   const url = `http://139.59.80.65/espin_api/cities`;
//
//   this.setState({ loading: true });
//
//   fetch(url)
//   .then(res => res.json())
//   .then(res => {
//    console.log(res.data);
//
//     const dd = res.data;
//     console.log(dd);
//     const newData = JSON.parse(dd);
//     console.log(newData);
//
//
//     this.setState({
//       data: res.data ,
//       error: res.error || null,
//       loading: false,
//       refreshing: false
//     });
//   })
//   .catch(error => {
//    this.setState({ error, loading: false });
//   });
// };


async onFetchLoginRecords() {

  this.setState({tapStatus:true});

    var data = {
     phoneNo: this.state.mNumber,
     cityId: this.state.cityId,
    };
    try {
     let response = await fetch(
      "http://139.59.80.65/espin_api/sms/sendotp",
      {
        method: "POST",
        headers: {
         "Accept": "application/json",
         "Content-Type": "application/json"
        },
       body: JSON.stringify(data)
     }
    );


     if (response.status >= 200 && response.status < 300) {
       this.setState({tapStatus:false});

       const myArrStr = response._bodyInit;
       const newData = JSON.parse(myArrStr)
       console.log(newData);
       const newData1 = newData.data
       const newData21 = newData1[0]
       AsyncStorage.setItem("userRef",newData21['userRef']);
       AsyncStorage.setItem("mob",this.state.mNumber);
       AsyncStorage.setItem("city",this.state.city);
       AsyncStorage.setItem("cityId",this.state.cityId.toString());



       const otpString = newData21['verificationCode'].toString()
       AsyncStorage.setItem("otp",otpString);
       Alert.alert(
         'Success',
         'Otp Sent to your Phone number',
         [
           {text: 'OK', onPress: () => {this.props.navigation.navigate('Item1')}},
         ],
         { cancelable: false }
       )


    }else {
      this.setState({tapStatus:false});

      Alert.alert("Alert","Invalid Phone number!" );

    }
   } catch (errors) {
     this.setState({tapStatus:false});

     Alert.alert(errors);
    }
}

componentDidMount(){
   return fetch('http://139.59.80.65/espin_api/cities')
     .then((response) => response.json())
     .then((responseJson) => {

       this.setState({
         isLoading: false,
         dataSource: responseJson.data,
       }, function(){

       });

     })
     .catch((error) =>{
       console.error(error);
     });
 }

componentWillUnmount(){




}

componentWillMount(){

  AsyncStorage.getItem("mob").then((value) => {
        console.log({"mob": value});

      this.setState({mobileNumber:value , mNumber:value});


      }).done();

      AsyncStorage.getItem("city").then((value) => {
            console.log({"city": value});
             if (!value) {
               this.setState({city:"Select city"});

            }else{
              this.setState({city:value});

            }


          }).done();

          AsyncStorage.getItem("cityId").then((value) => {
                console.log({"cityId": value});

              this.setState({cityId:value});


              }).done();
}
cityTapFunction=()=>{

this.setState({vHeight: this.state.vHeight === 0 ? 110:0})
this.setState({bWidth: this.state.bWidth === 0.0 ? 1.0:0.0})

}
loginFunction=()=>{

  const { mNumber, cityId } = this.state

  if (!cityId) {

    Alert.alert("Select City" )

  }else if (!mNumber) {

    Alert.alert("Enter Phone number" )

  }else {

    if (this.state.tapStatus == false ? this.onFetchLoginRecords():null);


  }
  //this.props.navigation.navigate('Item1')
}

selectCityAction=()=>{


Alert.alert("Floating Button Clicked");


}

_onPress = (re) => {
  this.setState({vHeight: this.state.vHeight === 0 ? 110:0 ,city:re.title,cityId:re.id})
  this.setState({bWidth: this.state.bWidth === 0.0 ? 1.0:0.0})

   //this.props.onPressItem(this.props.id);
 };
render() {
  return (
  <KeyboardAvoidingView keyboardVerticalOffset={offset} behavior = 'padding' style = {styles.container}><View style={styles.container}>

    <View style={{height: SCREEN_HEIGHT/2 - 20,width: SCREEN_WIDTH, backgroundColor:'green',alignItems: 'flex-start',justifyContent: 'flex-start'}}><Image style={{width: SCREEN_WIDTH+50,height: SCREEN_HEIGHT/2,left: 0}} source = {require('./Images/loginBack.jpg')} /></View>

  <View style={{height: SCREEN_HEIGHT/2 - 70,width: SCREEN_WIDTH,backgroundColor:'white',flexDirection: 'column',alignItems: 'center',alignSelf: 'baseline'}}>

    <Text style={{fontWeight: 'bold',fontSize: 20 , top: 5}}>ESPIN APP</Text>
    <Text style={{fontWeight: '200',fontSize: 16 , top: 10,width: 240,alignItems: 'center',}}>Please enter your phone number to login or create new account</Text>

    <View style={{top: 18,width:SCREEN_WIDTH-40,height: 150,backgroundColor: 'white'}}>

      <View style={{top: 3,width:SCREEN_WIDTH-40,height: 50,backgroundColor: 'white',flexDirection: 'row'}}>
        <Image style={{width: 50,height: 50,left: 0}} source = {require('./Images/location1.jpg')}/>
        <View style={{top: 0,width:SCREEN_WIDTH-90,height: 50,backgroundColor: 'white',flexDirection: 'row',borderWidth: 1,borderColor: 'grey'}}>

        <TouchableOpacity activeOpacity={0.5} onPress={this.cityTapFunction}><View style={{top: 0,width:SCREEN_WIDTH-140,height: 45,backgroundColor: 'white',left: 1,flexDirection: 'row',alignItems: 'center'}}>
          <Text style={{left: 8}}>{this.state.city}</Text>
  </View></TouchableOpacity>
<TouchableOpacity activeOpacity={0.5} onPress={this.cityTapFunction}><View style={{top: 0,width:46,height: 46,backgroundColor: 'white',alignItems: 'center',justifyContent: 'center'}}>
            <Image style={{width: 12,height: 10,}} source = {require('./Images/arrow.png')} />
          </View></TouchableOpacity>


        </View>

      </View>
      <View style={{top: 2,width:SCREEN_WIDTH-92,height: this.state.vHeight,backgroundColor: 'white',borderWidth: this.state.bWidth,borderColor: 'lightgrey',left: 52}}>
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) =>  <TouchableHighlight
      onPress={() => this._onPress(item)}><Text style={{justifyContent: 'flex-start',alignItems: 'flex-start',alignSelf: 'flex-start',fontSize: 18,left: 5}}>{item.title}</Text></TouchableHighlight>}
          keyExtractor={({id}, index) => id}
        />
      </View>
      <View style={{top: 15,width:SCREEN_WIDTH-40,height: 50,backgroundColor: 'blue',flexDirection: 'row'}}>
        <Image style={{width: 50,height: 50,left: 0}} source = {require('./Images/mobile.jpg')} /><View style={{top: 0,width:SCREEN_WIDTH-90,height: 50,backgroundColor: 'grey',flexDirection: 'row'}}>
          <TextInput placeholder = "Phone number"
            onChangeText={mNumber => this.setState({mNumber})}
  placeholderTextColor = 'lightgrey'
  keyboardType = 'numeric'
  returnKeyType='done'
  value = {this.state.mobileNumber}
  autoCorrect={false}
  ref = {(input)=> this.passwordInput = input}
  style = {styles.input2} />
      </View>
    </View>

    </View>
  </View>

  <TouchableOpacity  activeOpacity={0.5} onPress={this.loginFunction}><View style={{width:SCREEN_WIDTH-40,height: 50,backgroundColor: '#F27125',alignItems: 'center',justifyContent: 'center',}}><Text style={{color: 'white',fontWeight: '500',fontSize: 17}}>Get OTP</Text>
  </View></TouchableOpacity>
</View></ KeyboardAvoidingView>
  );
}
}

const styles = StyleSheet.create({
container: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#F5FCFF'
},
welcome: {
  fontSize: 20,
  textAlign: 'center',
  margin: 10,
},
instructions: {
  textAlign: 'center',
  color: '#333333',
  marginBottom: 5,
},

input :{
  height: 46,
  backgroundColor: 'white',
  marginBottom: 20,
  paddingHorizontal: 10,
  color: 'black',
  borderBottomWidth: 1,
  borderColor: 'white',
  borderRadius: 5 ,
  },
  input2 :{
    height: 48,
    backgroundColor: 'white',
    top:1,
    paddingHorizontal: 10,
    color: 'black',
    left:0,
    borderColor: 'white',
    borderRadius: 0 ,
    width: SCREEN_WIDTH-92
    },
});
