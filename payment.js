/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image
} from 'react-native';
const {width,height} = Dimensions.get('window')
import { DrawerActions } from 'react-navigation';

const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
export default class payment extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{top: 0,width: SCREEN_WIDTH,height: 70,flexDirection: 'row'}}><TouchableOpacity style={{marginTop: 35,marginLeft: 15}} onPress={()=>{this.props.navigation.dispatch(DrawerActions.openDrawer())}}><Image style={{width: 25 ,height: 20}} source={require('./Images/toggle.png')}/></TouchableOpacity>
        <View style={{width: width-60 ,justifyContent: 'center',alignItems: 'center',marginTop: 20,backgroundColor: 'clear'}}>
          <Text style={{color: 'black',justifyContent: 'center',alignItems: 'center',fontSize: 16,fontWeight: '500'}}>PAYMENT</Text>
        </View>

      </View>
      <View style={{width: width,height: 1,backgroundColor: '#dedede',top: 0}}></View>
      <View style={{width: width,height: 70,backgroundColor: 'white',top: 0,flexDirection: 'row',}}><Image style={{width: 30 ,height: 30,top: 20,left: 15}} source={require('./Images/paytm.png')}/>
      <Text style={{color: 'black',justifyContent: 'center',alignItems: 'center',fontSize: 16,fontWeight: '500',top: 25,left: 25}}>Paytm</Text><Image style={{width: 25 ,height: 20,top: 25,left: width-120}} source={require('./Images/right-arrow.png')}/>
    </View>


    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
