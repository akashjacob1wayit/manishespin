/* @flow */
// <Text style={styles.weekText}>
//     {rowData} {this.state.selectedID}
//   </Text>

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ListView,
  TouchableHighlight,
  TouchableOpacity,
  Image,
  Dimensions,
  AsyncStorage,
  Alert
} from 'react-native';
const {width,height} = Dimensions.get('window')
import { DrawerActions } from 'react-navigation';

const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width

export default class tripHistory extends Component {

    constructor(props) {
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 === r2
    });

    this.state = {
      userId:'',
      List:[],
      dataSource: ds.cloneWithRows([]),
      selectedID: 'tues',
    }




  }

 getUserRef=()=>{

   AsyncStorage.getItem("userRef").then((value) => {
         console.log({"userRef": value});
         this.setState({userId:value});
         if (!this.state.userId) {

         }else{
         this.getTrips();

         }

       }).done();


 }

  async getTrips() {

    var data = {
     userRef:this.state.userId
    };
    try {
     let response = await fetch(
      "http://139.59.80.65/espin_api/trips",
      {
        method: "POST",
        headers: {
         "Accept": "application/json",
         "Content-Type": "application/json"
        },
       body: JSON.stringify(data)
     }
    );


     if (response.status >= 200 && response.status < 300) {

       const myArrStr = response._bodyInit;
       const newData = JSON.parse(myArrStr)
       console.log(newData);
       const newData1 = newData.data
       const ds = new ListView.DataSource({
         rowHasChanged: (r1, r2) => r1 === r2
       });
this.setState({List:newData1,dataSource: ds.cloneWithRows(newData1),});
console.log(this.state.List);
this.newFunction();
    }else {
      Alert.alert("Alert","Try Again" );

    }
   } catch (errors) {
     Alert.alert(errors);
    }

}

  newFunction=()=>{
    console.log(this.state.List);



  }
  componentDidMount(){
    this.getUserRef();




}
  _getWeekList = () => {
    return this.state.list
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{top: 0,width: SCREEN_WIDTH,height: 70,flexDirection: 'row',backgroundColor: 'white'}}><TouchableOpacity style={{marginTop: 35,marginLeft: 15}} onPress={()=>{this.props.navigation.dispatch(DrawerActions.openDrawer())}}><Image style={{width: 25 ,height: 20}} source={require('./Images/toggle.png')}/></TouchableOpacity>
        <View style={{width: width-60 ,justifyContent: 'center',alignItems: 'center',marginTop: 20,backgroundColor: 'clear'}}>
          <Text style={{color: 'black',justifyContent: 'center',alignItems: 'center',fontSize: 16,fontWeight: '500'}}>TRIP HISTORY</Text></View></View>
        <View style={{width: width,height: height-70,backgroundColor: 'white'}}>
          <ListView showsHorizontalScrollIndicator={false} horizontal={false} dataSource={this.state.dataSource} renderRow={this._renderRow.bind(this)}></ListView>
        </View>
      </View>
    );
  }
  _renderRow(rowData, rowID) {
    return (
      <TouchableHighlight onPress={() => {
        this._selectedWeek(rowData, rowID)
      }}>
        <View style={styles.weekRow}>
          <View style={{width: SCREEN_WIDTH,height: 50,backgroundColor: '#f3f3f3',flexDirection: 'row'}}><View style={{height: 50,width: SCREEN_WIDTH/2,flexDirection: 'row'}}><Image style={{width: 25 ,height: 25,left: 10,top: 10}} source={require('./Images/bik.png')}/>
          <Text style={{left: 15,top: 15}}>August 25, 2018</Text></View>
          <View style={{height: 50,width: SCREEN_WIDTH/2,flexDirection: 'row',backgroundColor: '#f3f3f3'}}><View style={{top: 20,width: 8,height: 8,borderRadius:4,backgroundColor: 'green',left: SCREEN_WIDTH/2 - 100}}></View><Text style={{left: SCREEN_WIDTH/2 - 90,top: 15}}>Completed</Text></View>
        </View>
        <Text style={{left: 10,top: 5}}>BOOKING ID:{rowData.id}</Text>
        <View style={{top: 10,width: SCREEN_WIDTH,height: 172,backgroundColor: 'white',flexDirection: 'row'}}>
          <View style={{top: 0,left: 5,backgroundColor: 'white',height: 120,width: 30,alignItems: 'center',justifyContent: 'flex-start'}}><Image style={{width: 9 ,height: 100,top: 8}} source={require('./Images/locds2.png')}/></View>
          <View style={{left: 5,width: SCREEN_WIDTH-35,height:172,backgroundColor: 'white',flexDirection: 'row'}}>
            <View style={{width: SCREEN_WIDTH-120,height: 170,backgroundColor: 'white',flexDirection: 'column'}}>
              <Text style={{fontSize: 10,color: 'lightgrey',top: 3}}>Pick up Location</Text>
                <Text  numberOfLines={4} style={{fontSize: 13,color: 'black',top: 5, flexWrap: 'wrap'}}>{rowData.departure}</Text>

                  <Text style={{fontSize: 10,color: 'lightgrey',top: 65}}>Destination Location</Text>
                    <Text  numberOfLines={4} style={{fontSize: 13,color: 'black',top: 70, flexWrap: 'wrap'}}>{rowData.destination}</Text>

              </View>
            <View style={{width:90,height: 170,backgroundColor: 'white',flexDirection: 'column',alignItems:'center'}}><Image style={{width: 50 ,height: 50,top: 10,borderRadius: 25}} source={require('./Images/dog.png')}/></View>
          </View>
        </View>
        </View>
      </TouchableHighlight>
    );
  }
  _selectedWeek(rowData, rowID) {
    console.log('Selected week '+rowData);
    // this.setState({selectedID: rowData, dataSource: this.state.dataSource.cloneWithRows(this._getWeekList())});
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  weekRowSelected : {
  },
  weekRow: {
    height: 250,
    flexDirection: 'column'

  },
  weekTextSelected: {
    fontSize: 16,
    color: 'yellow',
  },
  weekText: {
    fontSize: 14,
    color: 'blue',

  }, descriptionContainerVer:{
    flex:0.5, //height (according to its parent)
    flexDirection: 'column', //its children will be in a row
    alignItems: 'center',
    backgroundColor: 'blue',
    // alignSelf: 'center',
  },
})
