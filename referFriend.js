/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image
} from 'react-native';
const {width,height} = Dimensions.get('window')
import { DrawerActions } from 'react-navigation';
import Share from 'react-native-share';
const shareOptions = {
    title: 'Share via',
    url: 'eSpin Referal Code',
    social: Share.Social
};
const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
export default class referFriend extends Component {
  SubmitAction=()=>{
    Share.open(shareOptions)
        .then((res) => { console.log(res) })
        .catch((err) => { err && console.log(err); });
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{top: 0,width: SCREEN_WIDTH,height: 70,flexDirection: 'row'}}><TouchableOpacity style={{marginTop: 35,marginLeft: 15}} onPress={()=>{this.props.navigation.dispatch(DrawerActions.openDrawer())}}><Image style={{width: 25 ,height: 20}} source={require('./Images/toggle.png')}/></TouchableOpacity>
        <View style={{width: width-60 ,justifyContent: 'center',alignItems: 'center',marginTop: 20,backgroundColor: 'clear'}}>
          <Text style={{color: 'black',justifyContent: 'center',alignItems: 'center',fontSize: 16,fontWeight: '500'}}></Text></View></View>
<Image style={{width: SCREEN_WIDTH ,height: 120}} source={require('./Images/refer.png')}/>
<View style={{backgroundColor: 'clear',justifyContent: 'center',alignItems: 'center',top: 5}}><Text style={{fontSize: 25,fontWeight:'bold'}}>REFER A FREIND</Text></View>
  <View style={{backgroundColor: 'clear',justifyContent: 'center',alignItems: 'center',top: 30}}><Text style={{fontSize: 16,fontWeight:'bold'}}>Share Your Invite Code</Text></View>

    <View style={{backgroundColor: 'clear',justifyContent: 'center',alignItems: 'center',top: 35,height: 60}}>
      <TouchableOpacity activeOpacity={0.5} onPress={this.SubmitAction}><View style={{top: 0,height: 60,width: 180,backgroundColor: 'clear',justifyContent: 'center',alignItems: 'center',flexDirection: 'row',borderWidth: 1.0,borderColor: 'grey'}}><View style={{top: 0,height: 40,width: 120,backgroundColor: 'clear',justifyContent: 'center',alignItems: 'center'}}>
        <Text style={{fontSize: 16}}>Invitation Code</Text>
    </View><Image style={{width: SCREEN_WIDTH ,height: 33,width: 31,left: 5}} source={require('./Images/share.png')}/>
</View></TouchableOpacity>
    </View>
  </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
