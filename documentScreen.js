
import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  Dimensions,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  TouchableHighlight,
  AsyncStorage,
  Alert
} from 'react-native';
const {width,height} = Dimensions.get('window')
const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
const ASPECT_RATIO = width/height

export default class documentScreen extends Component {

  constructor(props){
    super(props)

    this.state = {
        isVisible: false,
        leftMNew:-SCREEN_WIDTH,
        loading: false,
        data: [],
        page: 0,
        seed: 1,
        error: null,
        refreshing: false,
        items:[],
        city:'Choose ID document',
        bWidth:0.0,
        docNumber:'',
        documentId:'',
        vHeight:0,
        userRefString:''


      };

  }
  async postDocument() {

      var data = {
       documentNo: this.state.docNumber,
       documentId: this.state.documentId,
       userRef:this.state.userRefString
      };
      try {
       let response = await fetch(
        "http://139.59.80.65/espin_api/users/insertdoc",
        {
          method: "POST",
          headers: {
           "Accept": "application/json",
           "Content-Type": "application/json"
          },
         body: JSON.stringify(data)
       }
      );


       if (response.status >= 200 && response.status < 300) {

         const myArrStr = response._bodyInit;
         const newData = JSON.parse(myArrStr)
         console.log(newData);
         const newData1 = newData.data
         const newData21 = newData1[0]
         AsyncStorage.setItem("isDocumentUploaded",newData21['isDocumentUploaded'].toString());

         this.props.navigation.navigate('Main')

      }else {
        Alert.alert("Alert","Try Again" );

      }
     } catch (errors) {
       Alert.alert(errors);
      }
  }
  componentDidMount(){
     return fetch('http://139.59.80.65/espin_api/documents')
       .then((response) => response.json())
       .then((responseJson) => {

         this.setState({
           isLoading: false,
           dataSource: responseJson.data,
         }, function(){

         });

       })
       .catch((error) =>{
         console.error(error);
       });
   }

   skipAction=()=>{

this.props.navigation.navigate('Main')
}

  SubmitDoc=()=>{

    AsyncStorage.getItem("userRef").then((value) => {
          console.log({"userRef": value});
          this.setState({userRefString:value});
          if (!this.state.documentId) {
            Alert.alert("Alert","Select Document" );

          }else if (!this.state.docNumber) {
            Alert.alert("Alert","Enter document number" );

          }else{
          this.postDocument();

          }

        }).done();



  //  this.props.navigation.navigate('Main')
  }
  cityTapFunction=()=>{

  this.setState({vHeight: this.state.vHeight === 0 ? 110:0})
  this.setState({bWidth: this.state.bWidth === 0.0 ? 1.0:0.0})

  }
  _onPress = (re) => {
    this.setState({vHeight: this.state.vHeight === 0 ? 110:0 ,city:re.title,documentId:re.id})
    this.setState({bWidth: this.state.bWidth === 0.0 ? 1.0:0.0})

     //this.props.onPressItem(this.props.id);
   };
  render() {
    return (
      <KeyboardAvoidingView behavior = 'padding' style = {styles.container}><View style={styles.container}>
      <View style={{width: width,height: height/2 - 30,backgroundColor: 'red',alignItems: 'stretch'}}><Image style={{width: width,height: height/2,left: 0}} source = {require('./Images/tr.jpg')} /></View>

      <View style={{width: width,height: height/2 + 60,backgroundColor: 'white',flexDirection: 'column',alignItems:'center'}}>
      <Text style={{fontWeight: 'bold',fontSize: 22 , top: 10}}>CHOOSE A SECURITY CHECK</Text>
      <Text style={{fontWeight: '200',fontSize: 13 , top: 14,alignItems: 'center',}}>How do you want to confirm your identity?</Text>

      <View style={{top: 20,width:SCREEN_WIDTH-40,height: 300,backgroundColor: 'white'}}>

        <View style={{top: 10,width:SCREEN_WIDTH-40,height: 50,backgroundColor: 'white',flexDirection: 'row'}}>
          <Image style={{width: 52,height: 50,left: 0}} source = {require('./Images/1.png')}/>
          <View style={{top: 0,width:SCREEN_WIDTH-95,height: 50,backgroundColor: 'white',flexDirection: 'row',borderWidth: 1,borderColor: 'grey'}}>

          <TouchableOpacity activeOpacity={0.5} onPress={this.cityTapFunction}><View style={{top: 0,width:SCREEN_WIDTH-140,height: 45,backgroundColor: 'white',left: 1,flexDirection: 'row',alignItems: 'center'}}>
            <Text style={{left: 8}}>{this.state.city}</Text>
    </View></TouchableOpacity>

  <TouchableOpacity activeOpacity={0.5} onPress={this.cityTapFunction}><View style={{top: 0,width:40,height: 46,backgroundColor: 'white',alignItems: 'center',justifyContent: 'center'}}>
              <Image style={{width: 12,height: 10,}} source = {require('./Images/arrow.png')} />
            </View></TouchableOpacity>


          </View>

        </View>
        <View style={{top: 5,width:SCREEN_WIDTH-95,height: this.state.vHeight,backgroundColor: 'white',borderWidth: this.state.bWidth,borderColor: 'lightgrey',left: 52}}>
          <FlatList
            data={this.state.dataSource}
            renderItem={({item}) =>  <TouchableHighlight
        onPress={() => this._onPress(item)}><Text style={{justifyContent: 'flex-start',alignItems: 'flex-start',alignSelf: 'flex-start',fontSize: 18,left: 5}}>{item.title}</Text></TouchableHighlight>}
            keyExtractor={({id}, index) => id}
          />
        </View>
        <View style={{top: 20,width:SCREEN_WIDTH-40,height: 50,backgroundColor: 'blue',flexDirection: 'row'}}>
          <Image style={{width: 50,height: 50,left: 0}} source = {require('./Images/12.png')} /><View style={{top: 0,width:SCREEN_WIDTH-90,height: 50,backgroundColor: 'grey',flexDirection: 'row'}}>
            <TextInput placeholder = "Enter number"
              onChangeText={docNumber => this.setState({docNumber})}
    placeholderTextColor = 'lightgrey'
    returnKeyType = "done"
    ref = {(input)=> this.passwordInput = input}
    style = {styles.input2} />
        </View>
      </View>

      <TouchableOpacity style={{top: 40,width:SCREEN_WIDTH-40,height: 50}}  onPress={this.SubmitDoc}><View style={{top: 0,width:SCREEN_WIDTH-40,height: 50,backgroundColor: '#F27125',alignItems: 'center',justifyContent: 'center'}}><Text style={{color: 'white',fontWeight: '500',fontSize: 17,height: 30,top: 5}}>Continue</Text>
      </View></TouchableOpacity>

    <TouchableOpacity style={{top: 50,width:SCREEN_WIDTH-40,height: 50}}  activeOpacity={0.5} onPress={this.skipAction}><View style={{top: 0,width:SCREEN_WIDTH-40,height: 50,backgroundColor: '#F27125',alignItems: 'center',justifyContent: 'center'}}><Text style={{color: 'white',fontWeight: '500',fontSize: 17,height: 30,top: 5}}>Skip</Text>
      </View></TouchableOpacity>
      </View>





      </View>


      </View></KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input :{
    height: 46,
    backgroundColor: 'white',
    marginBottom: 20,
    paddingHorizontal: 10,
    color: 'black',
    borderBottomWidth: 1,
    borderColor: 'white',
    borderRadius: 5 ,
    },
    input2 :{
      height: 48,
      backgroundColor: 'white',
      top:1,
      paddingHorizontal: 10,
      color: 'black',
      left:0,
      borderColor: 'white',
      borderRadius: 0 ,
      width: SCREEN_WIDTH-92
      },
});
