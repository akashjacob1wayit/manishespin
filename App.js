/**
* Sample React Native App
* https://github.com/facebook/react-native
*
* @format
* @flow
*/

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Image,Dimensions,TouchableOpacity,Alert,TextInput,Keyboard,TouchableWithoutFeedback} from 'react-native';
import MapView from 'react-native-maps'
import { DrawerActions } from 'react-navigation';
import qrScannerCustom from './qrScannerCustom';
const {width,height} = Dimensions.get('window')

const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
const ASPECT_RATIO = width/height
const LATTITUDE_DELTA = 0.0922
const LONGTITUDE_DELTA = LATTITUDE_DELTA * ASPECT_RATIO
import { PermissionsAndroid } from 'react-native';




type Props = {};
export default class App extends Component<Props> {

  constructor(props){
    super(props)


        this.state={
          initialPosition:{
            latitude:0,
            longitude:0,
            latitudeDelta:0,
            longitudeDelta:0,
          },
          markerPosition:{
            latitude:0,
            longitude:0
          }


        }

  }


  componentDidMount(){
    navigator.geolocation.getCurrentPosition(
       (position) => {
         console.log("wokeeey");
         console.log(position);
         var lat = parseFloat(position.coords.latitude)
         var long = parseFloat(position.coords.longitude)

         var initialRegion = {
           latitude:lat,
           longitude:long,
           latitudeDelta: LATTITUDE_DELTA,
           longitudeDelta: LONGTITUDE_DELTA
         }

         this.setState({initialPosition:initialRegion})
         this.setState({markerPosition:position.coords})

       },
       (error) => this.setState({ error: error.message }),
       { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
     );

//5MWFOX
  }

  componentWillUnmount(){


  }

  rideNowAction=()=>{

    // Write your own code here, Which you want to execute on Floating Button Click Event.
    this.props.navigation.navigate("NestedNavigator");
    // Alert.alert("Floating Button Clicked");
    //this.props.navigation.navigate('Item1')
  }
  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}><View style={styles.container}>
        <View style={{top: 0,width: SCREEN_WIDTH,height: 70,flexDirection: 'row'}}><TouchableOpacity style={{marginTop: 35,marginLeft: 15}} onPress={()=>{this.props.navigation.dispatch(DrawerActions.openDrawer())}}><Image style={{width: 25 ,height: 20}} source={require('./Images/toggle.png')}/></TouchableOpacity>
        <View style={{width: width-60 ,justifyContent: 'center',alignItems: 'center',marginTop: 20,backgroundColor: '#f3f3f3'}}>
          <Text style={{color: 'black',justifyContent: 'center',alignItems: 'center',fontSize: 20,fontWeight: 'bold'}}>BOOK MY RIDE</Text>
        </View></View>

      <View style={{width: SCREEN_WIDTH,height: 160,flexDirection: 'row',backgroundColor: 'white',}}><View style={{width: 80,height: 160,backgroundColor: 'white',justifyContent: 'center',alignItems: 'center'}}><Image style={{}} source={require('./Images/123.jpg')}/></View>

      <View style={{width: width-80,height: 160,backgroundColor: 'white',flexDirection: 'column'}}>

<View style={{left: 0,top: 25,width: width-90,height: 50,backgroundColor: 'white',flexDirection: 'row',borderWidth: 0.5,borderColor: 'grey'}}><Image style={{top:15 ,width: 25,height: 25,left: 5}} source={require('./Images/search.png')}/>
<TextInput placeholder = "Enter Pick up Location"
placeholderTextColor = 'lightgrey'
returnKeyType = "go"
ref = {(input)=> this.passwordInput = input}
style = {styles.input2} /></View>

<View style={{left: 0,top: 40,width: width-90,height: 49,backgroundColor: 'white',flexDirection: 'row',borderWidth: 0.5,borderColor: 'grey'}}><Image style={{top:15 ,width: 25,height: 25,left: 5}} source={require('./Images/search.png')}/>
<TextInput placeholder = "Enter Drop Location"
placeholderTextColor = 'lightgrey'
returnKeyType = "go"
ref = {(input)=> this.passwordInput = input}
style = {styles.input2} /></View>
        </View>
      </View>

      <MapView
        style={{width: width,height: height - 280}}
        region={this.state.initialPosition}>

        <MapView.Marker
          coordinate={this.state.markerPosition}>
        </MapView.Marker>
      </MapView>

        <TouchableOpacity  activeOpacity={0.5} onPress={this.rideNowAction}><View style={{width:SCREEN_WIDTH,height: 50,backgroundColor: '#F27125',alignItems: 'center',justifyContent: 'center'}}><Text style={{color: 'white',fontWeight: '500',fontSize: 17}}>Scan now</Text>
      </View></TouchableOpacity>
    </View></TouchableWithoutFeedback>
  );
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f3f3f3',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  input2 :{
    height: 40,
    backgroundColor: 'white',
    top:5,
    paddingHorizontal: 10,
    color: 'black',
    left:2,
    borderColor: 'white',
    borderRadius: 0 ,
    width: width-120
    },
});
//
// <MapView.Marker
//   coordinate={this.state.markerPosition}>
//   <Image source={require('./Images/ICON2.png')} />
//
// </MapView.Marker>
