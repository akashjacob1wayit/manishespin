/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  Linking,
  AsyncStorage

} from 'react-native';
import { DrawerActions } from 'react-navigation';
import login from './login'
const {width,height} = Dimensions.get('window')

const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
const ASPECT_RATIO = width/height
export default class setting extends Component {

  FAQAction=()=>{
    Linking.canOpenURL('http://espin.demskigroup.com/faq').then(supported => {
          if (supported) {
            Linking.openURL('http://espin.demskigroup.com/faq');
          } else {
            console.log("Don't know how to open URI: ");
          }
        });

  }
  PRIVACYAction=()=>{

    Linking.canOpenURL('http://espin.demskigroup.com/privacy-policy').then(supported => {
          if (supported) {
            Linking.openURL('http://espin.demskigroup.com/privacy-policy');
          } else {
            console.log("Don't know how to open URI: ");
          }
        });
  }
  LOGOUTAction=()=>{
    AsyncStorage.setItem("userRef","");
    AsyncStorage.setItem("mob","");
    AsyncStorage.setItem("status","0");
    AsyncStorage.setItem("isDocumentUploaded","0");
    AsyncStorage.setItem("city","");
    AsyncStorage.setItem("cityId","");
    {this.props.navigation.navigate('Item0')};

  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{top: 0,width: SCREEN_WIDTH,height: 70,flexDirection: 'row'}}><TouchableOpacity style={{marginTop: 35,marginLeft: 15}} onPress={()=>{this.props.navigation.dispatch(DrawerActions.openDrawer())}}><Image style={{width: 25 ,height: 20}} source={require('./Images/toggle.png')}/></TouchableOpacity>
        <View style={{width: width-60 ,justifyContent: 'center',alignItems: 'center',marginTop: 20,backgroundColor: '#f3f3f3'}}>
          <Text style={{color: 'black',justifyContent: 'center',alignItems: 'center',fontSize: 20,fontWeight: 'bold'}}>Settings</Text>
        </View></View>
        <View style={{width: width,height: 1,backgroundColor: '#dedede',top: 0}}></View>
        <TouchableOpacity onPress={this.FAQAction}><View style={{width: width,height: 50,backgroundColor: 'white',marginTop: 8,flexDirection: 'row'}}><Image style={{width: 25 ,height: 25,left: 8,top: 10}} source={require('./Images/faq.png')}/>
          <Text style={{color: 'black',fontSize: 18,fontWeight: 'bold',marginLeft: 20,top: 13}}>FAQ</Text></View></TouchableOpacity>
          <TouchableOpacity onPress={this.PRIVACYAction}><View style={{width: width,height: 50,backgroundColor: 'white',marginTop: 8,flexDirection: 'row'}}><Image style={{width: 25 ,height: 25,left: 8,top: 10}} source={require('./Images/privacy.png')}/>
            <Text style={{color: 'black',fontSize: 18,fontWeight: 'bold',marginLeft: 20,top: 13}}>PRIVACY POLICY</Text>
        </View></TouchableOpacity>
      <TouchableOpacity onPress={this.LOGOUTAction}><View style={{width: width,height: 50,backgroundColor: 'white',marginTop: 8,flexDirection: 'row'}}><Image style={{width: 25 ,height: 25,left: 8,top: 10}} source={require('./Images/logout.png')}/>
              <Text style={{color: 'black',fontSize: 18,fontWeight: 'bold',marginLeft: 20,top: 13}}>LOGOUT</Text>
              </View></TouchableOpacity>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f3f3f3',

  },
});
