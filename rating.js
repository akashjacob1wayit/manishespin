/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  Dimensions,
  KeyboardAvoidingView,
  TouchableOpacity
} from 'react-native';

const {width,height} = Dimensions.get('window')
import { DrawerActions } from 'react-navigation';

const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width

import StarRatingBar from 'react-native-star-rating-view/StarRatingBar'

export default class rating extends Component {
  constructor(props){
    super(props)

    this.state = {
      massageString:'',
      scoreGot:'4',
      };

  }

  SubmitAction=()=>{

    console.log(this.state.scoreGot);
    console.log(this.state.scoreGot);


}
  render() {
    return (
        <KeyboardAvoidingView behavior = 'padding' style = {styles.container}><View style={styles.container}>
        <Text style={{fontSize: 24,alignSelf: 'center',marginTop: 65,fontWeight:'400'}}>YOUR LAST RIDE</Text>
        <Text  style={{fontSize: 14,alignSelf: 'center',marginTop: 10}}>August 24, 2018</Text>
        <View style={styles.innerView}><Text  style={{fontSize: 14,alignSelf: 'center',marginTop: 10}}>RATE YOUR APP</Text>
        <View style={{marginTop: 10,marginLeft: 0,marginRight: 0,height: 1,backgroundColor: "lightgrey"}}></View>
        <View style={{marginTop: 10,marginLeft: 0,marginRight: 0,height: 40,backgroundColor: "white",flexDirection: 'row'}}><Image style = {{width: 16,height:16,alignItems: 'center',alignSelf: 'flex-start',marginLeft: 10}} source = {require('./Images/circle.png')}/>
        <Text  style={{fontSize: 12,marginLeft:8}}>Plot e-195 Mohali</Text>
        </View>
        <View style={{marginTop: 0,marginLeft: 15,backgroundColor: "lightgrey",height: 5,width: 5,borderRadius: 2.5}}></View>
          <View style={{marginTop: 5,marginLeft: 15,backgroundColor: "lightgrey",height: 5,width: 5,borderRadius: 2.5}}></View>
            <View style={{marginTop: 5,marginLeft: 15,backgroundColor: "lightgrey",height: 5,width: 5,borderRadius: 2.5}}></View>
              <View style={{marginTop: 20,marginLeft: 0,marginRight: 0,height: 40,backgroundColor: "white",flexDirection: 'row'}}><Image style = {{width: 16,height:16,alignItems: 'center',alignSelf: 'flex-start',marginLeft: 10,tintColor:"red"}} source = {require('./Images/circle.png')}/>
              <Text  style={{fontSize: 12,marginLeft:8}}>Plot e-195 Mohali</Text>
              </View>
              <View style={{marginTop: 20,marginLeft: 0,marginRight: 0,height: 1,backgroundColor: "lightgrey"}}></View>
              <View  style={{marginTop: 20,marginLeft: 0,marginRight: 0,height: 40,backgroundColor: "white",flexDirection: 'row',justifyContent: 'center'}}><StarRatingBar
    readOnly={false}
    starStyle={{
            width: 26,
            height: 26,
        }}
    continuous={true}
    score={4}
    dontShowScore={true}
    onStarValueChanged={(score) => {
      this.state.scoreGot=score
    }}
/></View>
<View style = {styles.input2}><TextInput placeholder = "Leave a comment(optional)"
placeholderTextColor = 'black'
onChangeText={massageString => this.setState({massageString})}
numberOfLines={20}
returnKeyType = "done"
multiline={true}
ref = {(input)=> this.passwordInput = input}
style={{width:SCREEN_WIDTH-32,height: 85, backgroundColor: 'clear',marginLeft: 8}}/></View>

  <TouchableOpacity activeOpacity={0.5} onPress={this.SubmitAction}><View style={{width: width-40,height: 40,borderWidth: 0.0,marginTop: 0,alignItems: 'center',justifyContent: 'center',backgroundColor: 'green',marginLeft: -2}}><Text style={{color: 'white'}}>Submit</Text></View></TouchableOpacity>
  </View>

    </View></KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgrey'
   },
   innerView:{
     marginTop: 50,
     marginLeft: 20,
     marginRight: 20,
     height: 400,
     backgroundColor: "white",
     borderColor: 'lightgrey',
     borderWidth:1.5,
   shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0},
  input2 :{
    backgroundColor: 'white',
    marginTop:20,
    color: 'black',
    marginLeft: 0,
    marginRight: 0,
    borderColor: 'lightgrey',
    borderWidth:0.5,
    height: 85,
    alignItems: 'flex-start',
    textAlign:'left',
    textAlignVertical: "top",
    justifyContent: 'flex-start',
    flexWrap:'wrap'
    },
});
