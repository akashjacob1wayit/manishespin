/** @format */

import {AppRegistry,Dimensions} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import login from './login';
import stackNav from './stackNav';
import { DrawerNavigator } from 'react-navigation';
import SideMenu from './SideMenu';
import MainScreen from './MainScreen';
import splash from './splash';
import otpScreen from './otpScreen';
import documentScreen from './documentScreen';
import tripHistory from './tripHistory';
import support from './support';
import qrScannerCustom from './qrScannerCustom';
import rating from './rating';
const drawernav = DrawerNavigator({

  Item01: {
      screen:splash ,
      navigationOptions: {
      gesturesEnabled: false
    }
    },
  Item0: {
      screen:login ,
      navigationOptions: {
      gesturesEnabled: false
    }
    },
    Item1: {
        screen:otpScreen ,
        navigationOptions: {
        gesturesEnabled: false
      }
      },
  Item2: {
      screen: documentScreen,
      navigationOptions: {
      gesturesEnabled: false
    }
    },
    qrScanner: {
        screen: qrScannerCustom,
        navigationOptions: {
        gesturesEnabled: false
      }
    },
Item3: {
    screen: stackNav,
    navigationOptions: {
    gesturesEnabled: false
  }
  }
  }, {
    contentComponent: SideMenu,
    drawerWidth: Dimensions.get('window').width - 120,
    initialRouteName:'Item01'
});

 AppRegistry.registerComponent('eSpin', () => drawernav);
//AppRegistry.registerComponent('eSpin', () => App);
