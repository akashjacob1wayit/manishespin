import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, TouchableOpacity, Image
} from 'react-native';

import login from './login'
import documentScreen from './documentScreen';

import { StackNavigator } from  'react-navigation';
import IOSIcon from "react-native-vector-icons/Ionicons";
import MainScreen from "./MainScreen";
import tripHistory from './tripHistory';
import referFriend from './referFriend';
import payment from './payment'
import App from './App';
import support from './support';
import setting from './setting';
import accoountInformation from './accountInformation';
import qrScannerCustom from './qrScannerCustom';
import rating from './rating';

const stackNav = StackNavigator({
  Main : {
    screen: App,
    navigationOptions: ({navigation}) => ({
      title: "Home",
      headerLeft:(<TouchableOpacity onPress={() => navigation.navigate('DrawerOpen')}>
      <Image style = {{width:28 , height:18 , marginLeft: 10}}
      source = {require('/Volumes/My Computer/ReactNative New/eSpin/Images/mobile.jpg')}
      />
      </TouchableOpacity>
    ),
    headerStyle: {backgroundColor:'#0034A8'},
    header:null,
  initialRouteName : MainScreen,
  drawerWidth : 250 ,
  drawerPosition : 'left',
  drawerOpenRoute : 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute : 'DrawerToggle',
  drawerBackgroundColor : 'white',


  })
},
NestedNavigator: {
       screen: qrScannerCustom,
   },
   NestedNavigator2: {
          screen: rating,
      },
accounts: {
  screen: accoountInformation,
  navigationOptions: ({navigation}) => ({
    title: "Invoices",
    header: null,
    gesturesEnabled: false,
  })
},
TripHistory: {
  screen: tripHistory,
  navigationOptions: ({navigation}) => ({
    title: "Invoices",
    header: null,
    gesturesEnabled: false,
  })
},
referFriend: {
  screen: referFriend,
  navigationOptions: ({navigation}) => ({
    title: "Invoices",
    header: null,
    gesturesEnabled: false,


  })
},
payment: {
  screen: payment,
  navigationOptions: ({navigation}) => ({
    title: "Expenses",
    header: null,
    gesturesEnabled: false,


  })
},
support: {
  screen: support,
  navigationOptions: ({navigation}) => ({
    title: "Expenses",
    header: null,
    gesturesEnabled: false,


  })
},
setting: {
  screen: setting,
  navigationOptions: ({navigation}) => ({
    title: "Expenses",
    header: null,
    gesturesEnabled: false,


  })
},


});

export default stackNav;
